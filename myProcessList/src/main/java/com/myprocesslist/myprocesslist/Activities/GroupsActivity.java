package com.myprocesslist.myprocesslist.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.myprocesslist.myprocesslist.Adapter.GroupsListAdapter;
import com.myprocesslist.myprocesslist.DataBase.MyProcessListDBManager;
import com.myprocesslist.myprocesslist.Fragments.ProcessListFragment;
import com.myprocesslist.myprocesslist.General.AppManager;
import com.myprocesslist.myprocesslist.General.Constants;
import com.myprocesslist.myprocesslist.General.GroupItem;
import com.myprocesslist.myprocesslist.General.ProcessListItem;
import com.myprocesslist.myprocesslist.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupsActivity extends ActionBarActivity {

    private ListView mList;
    private GroupsListAdapter m_adapter;
    List<GroupItem> m_groupList;

    private MenuItem m_menuAdd;
    private MenuItem m_menuDelete;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    private String mGroupName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groups);
        getExtras();
        mTitle = getTitle();
        mList = (ListView) findViewById(android.R.id.list);
        mList.setClickable(true);
        mList.setItemsCanFocus(false);
        mList.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
                Intent resultIntent = new Intent();
                GroupItem item = (GroupItem) ((ListView)parent).getItemAtPosition(position);
                resultIntent.putExtra(Constants.GROUP_NAME_KEY, item.getGroupName());
                setResult(RESULT_OK, resultIntent);
                finish();
            }
        });
        m_groupList =  new ArrayList<GroupItem>();
        m_adapter = new GroupsListAdapter(this, View.VISIBLE);
        m_adapter.setListItems(m_groupList);
        refreshGroupList();
    }

    private void getExtras() {
        Intent intent = getIntent();
        mGroupName = intent.getStringExtra(Constants.GROUP_NAME_KEY);
    }
    private void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
        setMenuItems(mGroupName);
    }

    public void setMenuItems(String groupName) {
        m_menuDelete.setVisible(false);
        if(groupName.equalsIgnoreCase(getString(R.string.default_group_name))) {
            m_menuAdd.setVisible(true);
        }
        else {
            m_menuAdd.setVisible(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.process_list, menu);
        m_menuAdd = menu.findItem(R.id.action_add);
        m_menuDelete = menu.findItem(R.id.action_remove);
        restoreActionBar();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_add) {
            askUserForGroupName();
            return true;
        }
     /*   else if(id == R.id.action_remove) {
            String groupsName = getCheckedGroups();
            if(groupsName.length() > 0) {
                new DeleteGroupAsyncTask().execute(groupsName);
            }
        }*/
        return super.onOptionsItemSelected(item);
    }


    private String getCheckedGroups() {
        String groupsName = "";
        for(int i=0; i< mList.getCount(); i++) {
            GroupItem item = (GroupItem)mList.getItemAtPosition(i);
            if(item.Ischecked()) {
                groupsName += item.getGroupName() + ",";
            }
        }
        if(groupsName.length() < 2) {
            return "";
        }
        return groupsName.substring(0,groupsName.length()-1);
    }

    private void askUserForGroupName() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle(getString(R.string.add_group_title));
        alert.setMessage(getString(R.string.add_group_message));

        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String value = input.getText().toString();
                new AddGroupAsyncTask().execute(value);
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
            }
        });

        alert.show();
    }



    private void refreshGroupList() {

        mList.setAdapter(m_adapter);
        new LoadGroupsAsyncTask().execute();
    }

    /**
     * asyncTask to load the groups of the installed application
     */

    private class LoadGroupsAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {

            m_groupList =  AppManager.INSTANCE.getDbManger().getGroupsName();

            return null;
        }



        @Override
        protected void onPostExecute(Void result) {
            m_adapter.setListItems(m_groupList);
            m_adapter.notifyDataSetChanged();
        }
    }


    private class AddGroupAsyncTask extends AsyncTask<String, String, String > {

        @Override
        protected String doInBackground(String... names) {
            AppManager.INSTANCE.getDbManger().addGroup(names[0]);

            return names[0];
        }


        @Override
        protected void onPostExecute(final String result) {
            GroupItem item = new GroupItem(result);
            m_groupList.add(item);
            m_adapter.setListItems(m_groupList);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    m_adapter.notifyDataSetChanged();
                    String message = String.format(getApplicationContext().getString(R.string.group_was_added), result);
                    Toast.makeText(getApplicationContext(), message,
                            Toast.LENGTH_LONG).show();
                }
            });
        }

    }


}
