package com.myprocesslist.myprocesslist.Activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.myprocesslist.myprocesslist.DataBase.MyProcessListDBManager;
import com.myprocesslist.myprocesslist.Fragments.NavigationDrawerFragment;
import com.myprocesslist.myprocesslist.Fragments.ProcessListFragment;
import com.myprocesslist.myprocesslist.General.AppManager;
import com.myprocesslist.myprocesslist.General.Constants;
import com.myprocesslist.myprocesslist.General.ProcessListItem;
import com.myprocesslist.myprocesslist.R;

import java.util.ArrayList;


public class ProcessListActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, ProcessListFragment.ProcessListFragmentsCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    private MenuItem m_menuAdd;
    private MenuItem m_menuDelete;

    private String mGroupName;

    private ArrayList<ProcessListItem> m_checkedApps;


    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_process_list);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        mGroupName = getString(R.string.default_group_name);
        openProcessListFragment(mGroupName);


    }

    protected void onSaveInstanceState(Bundle outState) {
        ProcessListFragment processListFragment = (ProcessListFragment)getFragmentManager().findFragmentByTag(Constants.PROCESS_FRAGMENT_TAG);
        if(processListFragment != null) {
            m_checkedApps = processListFragment.getCheckedAppsUid();
            if(m_checkedApps != null && m_checkedApps.size() > 0) {
                Log.d(Constants.TAG, "ProcessListActivity::onOptionsItemSelected:: found " + m_checkedApps.size() + " checked apps" );
                AppManager.INSTANCE.setCheckedAppList(m_checkedApps);

                //TODO
                // replace appManger call with Parcelable
            }
        }
        super.onSaveInstanceState(outState);


    }
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        m_checkedApps = AppManager.INSTANCE.getCheckedAppList();
        AppManager.INSTANCE.clearCheckedAppList();

        //TODO
        // replace appManger call with Parcelable

    }

    @Override
    public void onNavigationDrawerItemSelected(String groupName) {
        // update the main content by replacing fragments
        mGroupName = groupName;
        setMenuItems(mGroupName);
        openProcessListFragment(groupName);
    }


    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
        setMenuItems(mGroupName);
    }

    public void setMenuItems(String groupName) {
        if(m_menuAdd != null && m_menuDelete != null) {
            if (groupName.equalsIgnoreCase(getString(R.string.default_group_name))) {
                m_menuDelete.setVisible(false);
                m_menuAdd.setVisible(true);
            } else {
                m_menuDelete.setVisible(true);
                m_menuAdd.setVisible(false);
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.process_list, menu);
            m_menuAdd = menu.findItem(R.id.action_add);
            m_menuDelete = menu.findItem(R.id.action_remove);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_add) {
            Intent intent = new Intent(this, GroupsActivity.class);
            intent.putExtra(Constants.GROUP_NAME_KEY, mGroupName);
            startActivityForResult(intent, Constants.GROUP_ACTIVITY_ADD_REQUEST_CODE);
            return true;
        }
        else if (id == R.id.action_remove) {
            ProcessListFragment processListFragment = (ProcessListFragment)getFragmentManager().findFragmentByTag(Constants.PROCESS_FRAGMENT_TAG);
            if(processListFragment != null) {

                processListFragment.deleteActionWasCalled();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == Constants.GROUP_ACTIVITY_ADD_REQUEST_CODE) {
            if(mNavigationDrawerFragment != null) {
                mNavigationDrawerFragment.refreshAppList();
            }
            if(data != null) {
                String groupName = data.getStringExtra(Constants.GROUP_NAME_KEY);

               new AddAppsToGroupAsyncTask().execute(groupName);
            }
        }
    }

    private void openProcessListFragment(String groupName) {

        Fragment processListFragment = ProcessListFragment.newInstance(groupName);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.container, processListFragment,Constants.PROCESS_FRAGMENT_TAG);
        transaction.addToBackStack(null);

        transaction.commit();

    }

    @Override
    public void onGroupWasDeleted() {
        if(mNavigationDrawerFragment != null) {
            mNavigationDrawerFragment.refreshAppList();
        }
        AppManager.INSTANCE.clearCheckedAppList();
        mGroupName = getString(R.string.default_group_name);
        setMenuItems(mGroupName);

        openProcessListFragment(mGroupName);
    }


    private class AddAppsToGroupAsyncTask extends AsyncTask<String, String, String > {

        @Override
        protected String doInBackground(String... names) {
            MyProcessListDBManager manager = AppManager.INSTANCE.getDbManger();
            for(ProcessListItem item: m_checkedApps) {
                manager.addApplicationToGroup(item.getAppUid(), item.getAppName(), item.getPackageName(), names[0]);
            }

            return names[0];
        }



        @Override
        protected void onPostExecute(final String result) {
            ProcessListFragment processListFragment = (ProcessListFragment)getFragmentManager().findFragmentByTag(Constants.PROCESS_FRAGMENT_TAG);
            if(processListFragment != null) {
                processListFragment.clearCheckedApps();
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.applications_were_added) + result,
                            Toast.LENGTH_LONG).show();
                }
            });
        }
    }


}
