package com.myprocesslist.myprocesslist.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;


import com.myprocesslist.myprocesslist.General.AppManager;
import com.myprocesslist.myprocesslist.General.GroupItem;
import com.myprocesslist.myprocesslist.General.ProcessListItem;
import com.myprocesslist.myprocesslist.R;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Created by Gidi on 10/06/2014.
 */
public class GroupsListAdapter extends BaseAdapter {

    private List<GroupItem> m_groupsList = null;
    private final Context mContext;
    private LayoutInflater mInflater;
    private int mcheckBoxVisability;


    public GroupsListAdapter(Context context, int checkBoxVisability) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mcheckBoxVisability = checkBoxVisability;
    }

    @Override
    public int getCount() {
        return m_groupsList.size();
    }

    @Override
    public Object getItem(int position) {
        return m_groupsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final GroupViewHolder holder;
        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.groups_item_list, null);

            holder = new GroupViewHolder();
            holder.mGroupName = (TextView) convertView.findViewById(R.id.group_name);
            holder.mIsChecked = (CheckBox) convertView.findViewById(R.id.isCheck_cb);
            holder.mIsChecked.setVisibility(View.GONE);
            holder.mIsChecked.setOnClickListener( new View.OnClickListener() {
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v ;
                    m_groupsList.get(position).setChecked(cb.isChecked());
                    holder.setIsChecked(cb.isChecked());
                }
            });
            convertView.setTag(holder);

        }
        else {
            // reuse/overwrite the view passed
            holder = (GroupViewHolder) convertView.getTag();
        }

        GroupItem item = m_groupsList.get(position);
        holder.setGroupName(item.getGroupName());

        return convertView;

    }

    public void setListItems(List<GroupItem> list) {
        m_groupsList = list;
    }

    public class GroupViewHolder {

        private TextView mGroupName;
        private CheckBox mIsChecked;

        public void setGroupName(String name) {
            mGroupName.setText(name);
        }
        public void setIsChecked(boolean isChecked) {
            mIsChecked.setChecked(isChecked);
        }

    }
}
