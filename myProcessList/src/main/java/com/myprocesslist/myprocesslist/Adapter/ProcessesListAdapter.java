package com.myprocesslist.myprocesslist.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


import com.myprocesslist.myprocesslist.General.AppManager;
import com.myprocesslist.myprocesslist.General.ProcessListItem;
import com.myprocesslist.myprocesslist.R;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Gidi on 10/06/2014.
 */
public class ProcessesListAdapter extends BaseAdapter {

    private List<ProcessListItem> m_processesList = null;
    private final Context mContext;
    private ImageView m_image = null;
    private List<ProcessListItem> m_list;
    private Map<String, Drawable> mIcons;
    private LayoutInflater mInflater;
    private Drawable mDeafultIcon;

    public ProcessesListAdapter(Context context) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mDeafultIcon = context.getResources().getDrawable(R.drawable.ic_launcher);
    }

    @Override
    public int getCount() {
      return m_processesList.size();
    }

    @Override
    public Object getItem(int position) {
        return m_processesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        final AppViewHolder holder;
        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.process_list_item_layout, null);

            holder = new AppViewHolder();
            holder.mAppName = (TextView) convertView.findViewById(R.id.application_name);
            holder.mPackageName = (TextView) convertView.findViewById(R.id.package_name);
            holder.mProcessId = (TextView) convertView.findViewById(R.id.process_id);
            holder.mIcon = (ImageView) convertView.findViewById(R.id.app_icon);
            holder.mIsChecked = (CheckBox) convertView.findViewById(R.id.isCheck_cb);
            holder.mIsChecked.setTag(position);
            holder.mIsChecked.setOnClickListener( new View.OnClickListener() {
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v ;
                  int index =   m_processesList.indexOf((ProcessListItem)cb.getTag());
                    m_processesList.get(index).setChecked(cb.isChecked());
                    if(cb.isChecked()) {
                        AppManager.INSTANCE.getCheckedAppList().add((ProcessListItem)cb.getTag());
                    }
                    else {
                        AppManager.INSTANCE.getCheckedAppList().remove((ProcessListItem)cb.getTag());
                    }
                    holder.setIsChecked(cb.isChecked());

                }
            });
            convertView.setTag(holder);

        }
        else {
            // reuse/overwrite the view passed
            holder = (AppViewHolder) convertView.getTag();
        }

        ProcessListItem item = m_processesList.get(position);
        holder.setAppName(item.getAppName());
        holder.setPackageName(item.getPackageName());
        holder.setAppUid(item.getAppUid());
        holder.setIsChecked(item.Ischecked());
        holder.mIsChecked.setTag(item);
        holder.setProcessId(Integer.toString(item.getProcessId()));
        if(mIcons == null || mIcons.get(item.getPackageName()) == null) {
            holder.setIcon(mDeafultIcon);
        }
        else {
            holder.setIcon(mIcons.get(item.getPackageName()));
        }
        return convertView;

    }

    public void setListItems(List<ProcessListItem> list) {
        m_processesList = list;
    }

    public void setIcons(Map<String, Drawable> icons) {
        this.mIcons = icons;

    }

    public class AppViewHolder {

        private ImageView mIcon;
        private TextView mAppName;
        private CheckBox mIsChecked;
        private TextView mProcessId;
        private TextView mPackageName;
        private int mUid;

        public void setAppName(String name) {
            mAppName.setText(name);
        }

        public void setProcessId(String id) {
            mProcessId.setText("pid: " + id);
        }

        public void setIcon(Drawable icon) {
            if(icon != null) {
                mIcon.setImageDrawable(icon);
            }
        }

        public void setIsChecked(boolean isChecked) {
            mIsChecked.setChecked(isChecked);
        }

        public void setAppUid(int uid) {
            mUid = uid;
        }

        public void setPackageName(String name) {
            mPackageName.setText("Package Name: " + name);
        }

        public int getAppUid(){
            return mUid;
        }
    }
}
