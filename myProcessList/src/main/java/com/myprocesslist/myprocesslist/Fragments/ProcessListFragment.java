package com.myprocesslist.myprocesslist.Fragments;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.myprocesslist.myprocesslist.Activities.ProcessListActivity;
import com.myprocesslist.myprocesslist.Adapter.ProcessesListAdapter;
import com.myprocesslist.myprocesslist.DataBase.MyProcessListDBManager;
import com.myprocesslist.myprocesslist.General.AppManager;
import com.myprocesslist.myprocesslist.General.Constants;
import com.myprocesslist.myprocesslist.General.ProcessListItem;
import com.myprocesslist.myprocesslist.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class ProcessListFragment extends Fragment implements AdapterView.OnItemClickListener {

    private ListView mList;

    private List<ProcessListItem> m_processesList = null;

    private ProcessesListAdapter m_adapter = null;

    private Activity mActivity;

    private String mGroupName;

    private ProcessListFragmentsCallbacks mCallbacks;

    public enum QuestionTypes {
        DeleteGroup
    }

    /**
     * Create a new instance of DetailsFragment, initialized to
     * show the text at 'index'.
     */
    public static ProcessListFragment newInstance(String  groupName) {
        ProcessListFragment f = new ProcessListFragment();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putString(Constants.mGroupNameKey, groupName);
        f.setArguments(args);
        return f;
    }

    public ProcessListFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        m_adapter = new ProcessesListAdapter(mActivity);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view =  inflater.inflate(R.layout.fragment_process_list, container, false);
        if(mActivity != null) {
            mList = (ListView) view.findViewById(android.R.id.list);
        }

        Bundle args = getArguments();
        mGroupName = args.getString(Constants.mGroupNameKey);

        if(mGroupName == "") {
            mGroupName = mActivity.getString(R.string.default_group_name);
        }

        TextView title = (TextView) view.findViewById(R.id.title_label);
        title.setText(mGroupName);

        mList.setClickable(true);
        mList.setOnItemClickListener(this);
        refreshAppList();

        return view;
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = (ProcessListFragmentsCallbacks) activity;
        mActivity = getActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
        mActivity = null;

    }



    private void refreshAppList() {
        if(m_processesList != null) {
            m_processesList.clear();
        }
        if(mGroupName.equalsIgnoreCase(mActivity.getString(R.string.default_group_name))) {
            loadInstalledApps();
            m_adapter.setListItems(m_processesList);
            mList.setAdapter(m_adapter);

            new LoadIconAsyncTask().execute(m_processesList.toArray(new ProcessListItem[] {}));
        }
        else {
            new LoadGroupsApssAsyncTask().execute(mGroupName);
        }

    }

    private void loadInstalledApps() {
        m_processesList = new ArrayList<ProcessListItem>();

        // getting all the information about installed application

        PackageManager packageManager = mActivity.getPackageManager();


        List<PackageInfo> packages = packageManager.getInstalledPackages(0);

        for(int i=0; i< packages.size(); i++) {
            PackageInfo packageInfo = packages.get(i);
            ApplicationInfo applicationInfo = packageInfo.applicationInfo;

            ProcessListItem processListItem = new ProcessListItem(applicationInfo.loadLabel(packageManager).toString(), getApplicationProcessId(applicationInfo.uid), packageInfo.packageName,
                    applicationInfo.uid);
            processListItem.setChecked(false);
            m_processesList.add(processListItem);
        }
    }

    public ArrayList<ProcessListItem> getCheckedAppsUid() {
   /*     ArrayList<ProcessListItem> checkedapps = new ArrayList<ProcessListItem>();
        for(int i = 0; i < mList.getCount(); i++) {
            ProcessListItem item =  (ProcessListItem)mList.getItemAtPosition(i);
            if(item.Ischecked()) {
                checkedapps.add(item);
            }
        }
*/
        return AppManager.INSTANCE.getCheckedAppList();
    }

    public void clearCheckedApps() {
        for (int i = 0; i < mList.getCount(); i++) {
            ProcessListItem item = (ProcessListItem) mList.getItemAtPosition(i);
            item.setChecked(false);
        }
        AppManager.INSTANCE.clearCheckedAppList();
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                m_adapter.notifyDataSetChanged();
            }
        });
    }


    private int getApplicationProcessId(int uid) {

        ActivityManager manager = (ActivityManager) mActivity.getSystemService(mActivity.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> services= manager.getRunningAppProcesses();

        for(int i=0; i< services.size(); i++) {
            if( services.get(i).uid == uid) {
                return services.get(i).pid;
            }
        }
        return 0;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final  ProcessListItem item = (ProcessListItem) parent.getItemAtPosition(position);
        CheckBox cb = (CheckBox) view.findViewById(R.id.isCheck_cb);
        boolean isChecked = !cb.isChecked();
        cb.setChecked(isChecked);
        item.setChecked(isChecked);
    }

    private void AskYourQuestion(String question, final QuestionTypes type ) {
        AlertDialog.Builder alert = new AlertDialog.Builder(mActivity);

        alert.setTitle("");
        alert.setMessage(question);

        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if(type == QuestionTypes.DeleteGroup) {
                    new DeleteGroupAsyncTask().execute(mGroupName);
                }
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
            }
        });

        alert.show();
    }
    private static int[] convertIntegers(List<Integer> integers)
    {
        int[] ret = new int[integers.size()];
        Iterator<Integer> iterator = integers.iterator();
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = iterator.next().intValue();
        }
        return ret;
    }

    public void deleteActionWasCalled() {

        ArrayList<ProcessListItem> items = getCheckedAppsUid();
        if (items.size() == 0) {
            String question = "Do you wish to delete the group " + mGroupName + " ?";
            AskYourQuestion(question, QuestionTypes.DeleteGroup);
        } else {
            new RemoveAppFromGroupAsyncTask(items).execute(mGroupName);
        }
    }

    /**
     * asyncTask to load the icons of the installed application
     */

    private class LoadIconAsyncTask extends AsyncTask<ProcessListItem, Void, Void> {

        @Override
        protected Void doInBackground(ProcessListItem... processListItems) {
            Map<String, Drawable> icons = new HashMap<String, Drawable>();
            PackageManager packageManager = getActivity().getPackageManager();

            for(ProcessListItem item : processListItems) {
                String name = item.getPackageName();
                Drawable icon = null;
                try {
                    Intent intent = packageManager.getLaunchIntentForPackage(name);
                    if(intent != null) {
                        icon = packageManager.getActivityIcon(intent);
                    }
                }
                catch (PackageManager.NameNotFoundException e) {
                    Log.d(Constants.TAG, "LoadIconAsyncTask::doInBackground:: couldn't find icon for package " + name + " :: " + e.getMessage());
                }

                icons.put(name, icon);
            }

            m_adapter.setIcons(icons);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            m_adapter.notifyDataSetChanged();
        }
    }

    private class LoadGroupsApssAsyncTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... names) {
            MyProcessListDBManager dbManager = AppManager.INSTANCE.getDbManger();
            m_processesList = dbManager.getGroupsApps(names[0]);

            PackageManager packageManager = mActivity.getPackageManager();

            for (ProcessListItem item : m_processesList) {

                try {
                    PackageInfo packageInfo = packageManager.getPackageInfo(item.getPackageName(), PackageManager.GET_ACTIVITIES);
                    item.setAppPid(getApplicationProcessId(item.getAppUid()));
                } catch (PackageManager.NameNotFoundException e) {
                    Log.d(Constants.TAG, "LoadGroupsApssAsyncTask::doInBackground:: application: " + item.getAppName() + " is not installed");
                    int[] uids = new int[1];
                    uids[0] = item.getAppUid();
                    dbManager.removeApplicationsFromGroup(uids, names[0]);
                    m_processesList.remove(item);
                }

            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            m_adapter.setListItems(m_processesList);
            mList.setAdapter(m_adapter);

            new LoadIconAsyncTask().execute(m_processesList.toArray(new ProcessListItem[]{}));
        }
    }

    private class RemoveAppFromGroupAsyncTask extends AsyncTask<String, Void, Void> {
        ArrayList<ProcessListItem> m_items;

        public RemoveAppFromGroupAsyncTask(ArrayList<ProcessListItem> items) {
            m_items = items;
        }

        @Override
        protected Void doInBackground(String... names) {
            MyProcessListDBManager dbManager = AppManager.INSTANCE.getDbManger();
            ArrayList<Integer> uids = new ArrayList<Integer>();
            for (ProcessListItem item : m_items) {
                uids.add(item.getAppUid());
                m_processesList.remove(item);
            }
            dbManager.removeApplicationsFromGroup(convertIntegers(uids), names[0]);


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            AppManager.INSTANCE.clearCheckedAppList();
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    m_adapter.notifyDataSetChanged();
                }
            });

        }
    }

    private class DeleteGroupAsyncTask extends AsyncTask<String, String, String > {

        @Override
        protected String doInBackground(String... names) {
            AppManager.INSTANCE.getDbManger().deleteGroups(names[0]);

            return names[0];
        }


        @Override
        protected void onPostExecute(final String result) {
            if(mCallbacks != null) {
                mCallbacks.onGroupWasDeleted();
            }
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    m_adapter.notifyDataSetChanged();
                    String message = String.format(mActivity.getString(R.string.groups_were_delete), result);
                    Toast.makeText(mActivity, message,
                            Toast.LENGTH_LONG).show();
                }
            });
        }

    }

    /**
     * Callbacks interface that all activities using this fragment must implement.
     */
    public static interface ProcessListFragmentsCallbacks {
        /**
         * Called when an item in the navigation drawer is selected.
         */
        void onGroupWasDeleted();
    }
}
