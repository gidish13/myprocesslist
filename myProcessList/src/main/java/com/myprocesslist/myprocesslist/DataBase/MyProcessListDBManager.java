package com.myprocesslist.myprocesslist.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


import com.myprocesslist.myprocesslist.General.Constants;
import com.myprocesslist.myprocesslist.General.GroupItem;
import com.myprocesslist.myprocesslist.General.ProcessListItem;
import com.myprocesslist.myprocesslist.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Created by Gidi on 25/05/14.
 */
public class MyProcessListDBManager {
    private SQLiteDatabase database;
    private MyProcessListDBHelper dbHelper;
    private Context mContext;
    private final int ITEM_DOESNT_EXISTS = -1;


    public MyProcessListDBManager(Context context) {
        dbHelper = new MyProcessListDBHelper(context);
        mContext = context;
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void addGroup(String name) {
        if(isGroupExists(name) != ITEM_DOESNT_EXISTS) {
            return;
        }
        database.execSQL("insert into " + MyProcessListDBHelper.TABLE_GROUPS + "(" + MyProcessListDBHelper.GROUP_NAME + ")  VALUES (?)", new Object[] {
                name
        });
    }

    public int isGroupExists(String name) {
        String selectQuery = "SELECT " + MyProcessListDBHelper.GROUP_KEY_ID  + " FROM " + MyProcessListDBHelper.TABLE_GROUPS + " where " + MyProcessListDBHelper.GROUP_NAME +
                " = ?"  ;
        Cursor cursor = database.rawQuery(selectQuery, new String[] {name});


        if(cursor.moveToFirst()) {
            return cursor.getInt(0);
        }

        return ITEM_DOESNT_EXISTS;
    }

    public boolean isAppExistsInGroup(int uid, int groupid) {
        String selectQuery = "SELECT " + MyProcessListDBHelper.APP_NAME  + " FROM " + MyProcessListDBHelper.TABLE_INSTALLED_APPLICATIONS + " where " +
                MyProcessListDBHelper.APP_UID + " = ? and " +  MyProcessListDBHelper.GROUP_KEY_ID + " = ?" ;
        Cursor cursor = database.rawQuery(selectQuery, new String[] {uid+"", groupid+""});


        if(cursor.moveToFirst()) {
            return true;
        }

        return false;
    }



    public List<GroupItem> getGroupsName() {

        final List<GroupItem> groupItems = new ArrayList<GroupItem>();
        String selectQuery = "SELECT " + MyProcessListDBHelper.GROUPS_COLUMNS  + " FROM " + MyProcessListDBHelper.TABLE_GROUPS;
        Cursor cursor = database.rawQuery(selectQuery, null);

     //   return all groups stored in the db
        while (cursor.moveToNext()) {
            GroupItem item = new GroupItem(cursor.getString(1));
            item.setGroupId(cursor.getInt(0));
            groupItems.add(item);
        }

        return groupItems;
    }

    public List<ProcessListItem> getGroupsApps(String groupName) {
        int groupId = isGroupExists(groupName);
        if (groupId == ITEM_DOESNT_EXISTS) {
            Log.d(Constants.TAG, "MyProcessListDBManager::addApplicationToGroup:: the group " + groupName + " doesn't exists");
            return null;
        }
        final List<ProcessListItem> processListItems = new ArrayList<ProcessListItem>();
        String selectQuery = "SELECT " + MyProcessListDBHelper.INSTALLED_APPLICATIONS_COLUMNS  + " FROM " + MyProcessListDBHelper.TABLE_INSTALLED_APPLICATIONS
                + " where " + MyProcessListDBHelper.GROUP_KEY_ID + " = " + Integer.toString(groupId);
        Cursor cursor = database.rawQuery(selectQuery, null);

        //   return all groups stored in the db
        while (cursor.moveToNext()) {
            ProcessListItem item = new ProcessListItem(cursor.getString(1),0,cursor.getString(2),cursor.getInt(0));

            processListItems.add(item);
        }

        return processListItems;
    }

    public int addApplicationToGroup(int appUid, String appName, String appPackageName, String groupName) {
        int groupId = isGroupExists(groupName);
        if (groupId == ITEM_DOESNT_EXISTS) {
            Log.d(Constants.TAG, "MyProcessListDBManager::addApplicationToGroup:: the group " + groupName + " doesn't exists");
            return ITEM_DOESNT_EXISTS;
        }
        if(isAppExistsInGroup(appUid, groupId)) {
            return groupId;
        }
        database.execSQL("insert into " + MyProcessListDBHelper.TABLE_INSTALLED_APPLICATIONS + "(" + MyProcessListDBHelper.INSTALLED_APPLICATIONS_COLUMNS + ")  VALUES (?,?,?,?)", new Object[]{
                appUid, appName, appPackageName, groupId});

        return groupId;
    }

    public int removeApplicationsFromGroup(int[] uids, String groupName) {
        int groupId = isGroupExists(groupName);
        if (groupId == ITEM_DOESNT_EXISTS) {
            Log.d(Constants.TAG, "MyProcessListDBManager::addApplicationToGroup:: the group " + groupName + " doesn't exists");
            return ITEM_DOESNT_EXISTS;
        }
        String deleteQuery = "delete from " + MyProcessListDBHelper.TABLE_INSTALLED_APPLICATIONS  + " where " + MyProcessListDBHelper.APP_UID +
                " in ( " + createQueryValues(uids.length) + ") and " + MyProcessListDBHelper.GROUP_KEY_ID + " = ?"  ;


        Object[] objectArr = new Object[uids.length + 1];
        for(int i=0 ; i< uids.length; i++) {
            objectArr[i] = uids[i];
        }
        objectArr[objectArr.length-1] = groupId;
        database.execSQL(deleteQuery, objectArr);

        return groupId;
    }


    public void deleteGroup(String groupName) {
        int groupId = isGroupExists(groupName);
        if (groupId == ITEM_DOESNT_EXISTS) {
            Log.d(Constants.TAG, "MyProcessListDBManager::addApplicationToGroup:: the group " + groupName + " doesn't exists");
            return;
        }

        // first remove apps related to this group
        String deleteQuery = "delete from " + MyProcessListDBHelper.TABLE_INSTALLED_APPLICATIONS  + " where " + MyProcessListDBHelper.GROUP_KEY_ID + " = ?"  ;
        database.execSQL(deleteQuery, new Object[]{ groupId});

        // delete the group
        deleteQuery = "delete from " + MyProcessListDBHelper.TABLE_GROUPS  + " where " + MyProcessListDBHelper.GROUP_KEY_ID + " = ?"  ;
        database.execSQL(deleteQuery, new Object[]{ groupId});

    }

    public void deleteGroups(String groupsName) {
        String groupsId = "";
        String selectQuery = "SELECT " + MyProcessListDBHelper.GROUP_KEY_ID  + " FROM " + MyProcessListDBHelper.TABLE_GROUPS + " where " +
                MyProcessListDBHelper.GROUP_NAME + " in ( " + createQueryValues(groupsName.length()) + " )";
        Cursor cursor = database.rawQuery(selectQuery, groupsName.split(","));

        //   return all groups stored in the db
        while (cursor.moveToNext()) {
            groupsId += cursor.getString(0) + ",";
        }
        groupsId = groupsId.substring(0,groupsId.length() - 1);
        String queryValues = createQueryValues(groupsId.length());
        // first remove apps related to this group
        String deleteQuery = "delete from " + MyProcessListDBHelper.TABLE_INSTALLED_APPLICATIONS  + " where " + MyProcessListDBHelper.GROUP_KEY_ID +
                " in ( " + queryValues  + " )";
        database.execSQL(deleteQuery, new Object[]{ groupsId});

        // delete the group
        deleteQuery = "delete from " + MyProcessListDBHelper.TABLE_GROUPS  + " where " + MyProcessListDBHelper.GROUP_KEY_ID +
                " in ( " + queryValues + " )";
        database.execSQL(deleteQuery, new Object[]{ groupsId});

    }

    private String createQueryValues(int len) {
        if (len < 1) {
            // It will lead to an invalid query
            throw new RuntimeException("No placeholders");
        } else {
            StringBuilder sb = new StringBuilder(len * 2 - 1);
            sb.append("?");
            for (int i = 1; i < len; i++) {
                sb.append(",?");
            }
            return sb.toString();
        }

    }



}
