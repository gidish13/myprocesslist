package com.myprocesslist.myprocesslist.DataBase;

import android.content.Context;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.myprocesslist.myprocesslist.General.AppManager;

/**
 * Created by Gidi on 25/05/14.
 */
public class MyProcessListDBHelper extends SQLiteOpenHelper {
    private static String DB_NAME = "myInstalledApplications.db";
    private static int DB_VERSION = 1;

    public static final String TABLE_INSTALLED_APPLICATIONS = "applications";
    public static final String TABLE_GROUPS = "groups";

    // Common column names
    public static final String APP_ID = "app_id";
    public static final String APP_UID = "app_uid";
    public static final String APP_NAME = "app_name";
    public static final String APP_PACKAGE_NAME = "app_package_name";
    public static final String GROUP_KEY_ID = "group_id";
    public static final String GROUP_NAME = "name";


    //user_settings column names
    public static final String INSTALLED_APPLICATIONS_COLUMNS =   APP_UID + "," + APP_NAME + "," + APP_PACKAGE_NAME + "," + GROUP_KEY_ID;
    public static final String GROUPS_COLUMNS =   GROUP_KEY_ID + "," + GROUP_NAME ;




    // Create Table queries
    private static final String CREATE_TABLE_APPLICATIONS = "CREATE TABLE "
            + TABLE_INSTALLED_APPLICATIONS + "(" + APP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + APP_UID + " INTEGER NOT NULL," +
            APP_NAME + " TEXT, "  + APP_PACKAGE_NAME + " TEXT,"  + GROUP_KEY_ID + " INTEGER)";

    private static final String CREATE_TABLE_GROUPS = "CREATE TABLE "
            + TABLE_GROUPS + "(" + GROUP_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + GROUP_NAME
            + " TEXT)";


    public MyProcessListDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public MyProcessListDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_TABLE_GROUPS);
        db.execSQL(CREATE_TABLE_APPLICATIONS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i2) {
        db.execSQL("DROP TABLE IF EXISTS " +  TABLE_INSTALLED_APPLICATIONS);
        db.execSQL("DROP TABLE IF EXISTS " +  TABLE_GROUPS);
        onCreate(db);
    }
}
