package com.myprocesslist.myprocesslist.General;

/**
 * Created by Gidi on 10/06/2014.
 */
public class Constants {

    public static final String TAG = "myProcessList";

    public static String mGroupNameKey = "groupname";

    public static int GROUP_ACTIVITY_ADD_REQUEST_CODE = 100;

    public static int GROUP_ACTIVITY_REMOVE_REQUEST_CODE = 101;

    public static String GROUP_NAME_KEY = "group_name";

    public static String PROCESS_FRAGMENT_TAG = "ProcessListFragment";

    public static String DRAWER_FRAGMENT_TAG = "DrawerFragment";

    public static String CHECKED_APPS_KEY = "checkedapps_key";

}
