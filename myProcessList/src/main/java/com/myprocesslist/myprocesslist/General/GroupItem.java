package com.myprocesslist.myprocesslist.General;

/**
 * Created by Gidi on 11/06/2014.
 */
public class GroupItem {

    private String mGroupName;
    private int mGroupId;
    private boolean m_IsChecked =  false;

    public GroupItem(String groupName) {
        super();
        this.mGroupName = groupName;

    }

    public String getGroupName() {
        return mGroupName;
    }

    public void setGroupId(int id) {
        mGroupId = id;
    }

    public int getGroupId() {
        return mGroupId;
    }

    public boolean Ischecked() {
        return m_IsChecked;
    }

    public void setChecked(boolean ischecked) {
        m_IsChecked = ischecked;
    }
}
