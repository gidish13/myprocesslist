package com.myprocesslist.myprocesslist.General;

import android.app.Application;
import android.util.Log;



import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.myprocesslist.myprocesslist.DataBase.MyProcessListDBManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gidi on 10/06/14.
 */
public final class AppManager extends Application {

    @Override
    public void onCreate() {
      super.onCreate();
      INSTANCE = this;

      Log.d(Constants.TAG, "*********************INITIALIZING APPLICATION*********************");

  }


    public MyProcessListDBManager getDbManger() {
        if(mDbManger == null) {
            mDbManger = new MyProcessListDBManager(this);
        }
        return mDbManger;
    }

    public static AppManager INSTANCE;
    private MyProcessListDBManager mDbManger = null;
    private ArrayList<ProcessListItem> mCheckedAppsList = null;

    public void setCheckedAppList(ArrayList<ProcessListItem> checkedAppsList) {
        mCheckedAppsList = checkedAppsList;
    }

    public ArrayList<ProcessListItem> getCheckedAppList() {
        if(mCheckedAppsList == null) {
            mCheckedAppsList = new ArrayList<ProcessListItem>();
        }
        return mCheckedAppsList;
    }

    public void clearCheckedAppList() {
        if(mCheckedAppsList != null) {
            mCheckedAppsList.clear();
        }
    }
}
