package com.myprocesslist.myprocesslist.General;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

/**
 * Created by Gidi on 10/06/2014.
 */
public class ProcessListItem {

    private String m_appName;
    private String m_packageName;
    private int m_processId;
    private Drawable m_appIcon;
    private boolean m_IsChecked;
    private int mUid;

    public ProcessListItem(String appName, int processId, String packagename, int uid) {
        super();
        this.m_appName = appName;
        this.m_processId = processId;
        this.m_packageName = packagename;
        this.mUid = uid;
    }

    public String getAppName() {
        return m_appName;
    }

    public String getPackageName() {
        return m_packageName;
    }

    public int getAppUid() {
        return mUid;
    }

    public int getProcessId() {
        return m_processId;
    }

    public Drawable getAppIcon() {
        return m_appIcon;
    }

    public void setAppIcon(Drawable icon) {
         m_appIcon = icon;
    }

    public boolean Ischecked() {
        return m_IsChecked;
    }

    public void setChecked(boolean ischecked) {
        m_IsChecked = ischecked;
    }

    public void setAppPid(int pid) {
        m_processId = pid;
    }
}
